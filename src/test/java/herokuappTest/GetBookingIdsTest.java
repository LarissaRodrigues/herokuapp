package herokuappTest;

import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.hamcrest.Matchers;
import org.testng.annotations.Test;
import tasks.Tasks;
import utils.BaseTest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.registerParser;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

public class GetBookingIdsTest extends BaseTest {

   Tasks task = new Tasks();

    @Test(groups = "suite")
    public void getBooking(){
        task.getBookingId()
                .statusCode(HttpStatus.SC_OK)
                .contentType(ContentType.JSON)
                .body(matchesJsonSchema(new File("src/test/resources/json_schemas/booking_schema.json")))
                .body("firstname", Matchers.is("Jim"));
    }

    @Test(groups = "suite")
    public void getBookingInvalid(){
        registerParser("text/plain", Parser.TEXT);
        task.getBookingIdInvalid()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .body(equalTo("Not Found"));
    }

    @Test(groups = "suite")
    public void getBookingSize(){
        int size =task.getBooking().extract().jsonPath().getList("$").size();
        task.getBooking().statusCode(HttpStatus.SC_OK).body("size()", is(size));
    }
}

