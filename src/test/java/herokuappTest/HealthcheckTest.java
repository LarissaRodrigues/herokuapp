package herokuappTest;

import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import org.testng.annotations.Test;
import tasks.Tasks;
import utils.BaseTest;

import static org.hamcrest.Matchers.equalTo;

public class HealthcheckTest extends BaseTest {

    @Test(groups = "suite")
    public void healthcheckTest(){
        RestAssured.registerParser("text/plain", Parser.TEXT);
        Tasks.getHealthcheck().statusCode(201).body(equalTo("Created"));
    }
}
