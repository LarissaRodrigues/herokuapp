package herokuappTest;

import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import tasks.Tasks;
import utils.BaseTest;

import java.util.HashMap;
import java.util.Map;

public class PutBookingIdsTest extends BaseTest {

    Tasks task = new Tasks();

    @Test(groups = "suite")
    public void putBooking(){
        Map<String, Object> dados = new HashMap<String, Object>();
        dados.put("firstname", "TESTEPUT");
        dados.put("lastname", "Rodrigues");
        dados.put("totalprice", 111);
        dados.put("depositpaid", true);

        Map<String, Object> bookingdatesDados = new HashMap<String, Object>();
        bookingdatesDados.put("checkin", "2018-01-01");
        bookingdatesDados.put("checkout", "2019-01-01");

        dados.put("bookingdates", bookingdatesDados);
        dados.put("additionalneeds", "Breakfast");

        task.putBookingId(dados).statusCode(HttpStatus.SC_OK);
    }
}
