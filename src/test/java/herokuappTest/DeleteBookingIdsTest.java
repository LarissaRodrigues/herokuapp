package herokuappTest;

import io.restassured.parsing.Parser;
import org.apache.http.HttpStatus;
import org.testng.annotations.Test;
import tasks.Tasks;
import utils.BaseTest;

import static io.restassured.RestAssured.registerParser;
import static org.hamcrest.Matchers.equalTo;

public class DeleteBookingIdsTest extends BaseTest {

    Tasks task = new Tasks();

    @Test
    public void deleteBookin(){
        registerParser("text/plain", Parser.TEXT);
        task.deleteBookingId(93)
                .statusCode(HttpStatus.SC_CREATED)
                .body(equalTo("Created"));
    }

    @Test
    public void deleteBookinInvalidID(){
        registerParser("text/plain", Parser.TEXT);
        task.deleteBookingId("test")
                .statusCode(HttpStatus.SC_METHOD_NOT_ALLOWED)
                .body(equalTo("Method Not Allowed"));
    }
}
