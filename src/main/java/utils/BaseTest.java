package utils;

import api.report.ReportListener;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

import static io.restassured.RestAssured.baseURI;

@Listeners({ReportListener.class})
public class BaseTest {

    @BeforeClass(groups = "suite")
    public void preCondiction(){
        baseURI = "https://treinamento-api.herokuapp.com/";
    }

}
