package tasks;

import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;
import utils.Log;

import java.util.Map;

public class Tasks {

    public static ValidatableResponse getHealthcheck(){
        return RestAssured.given().when().get("ping").then();
    }

    public ValidatableResponse getBookingId(){
        Log.info("Realizando uma requisição");
        return RestAssured.given()
                .log().all()
                .pathParam("id", 63)
                .when()
                .get("booking/{id}")
                .then();
    }

    public ValidatableResponse getBooking(){
        Log.info("Realizando uma requisição");
        return RestAssured.given()
                .log().all()
                .when()
                .get("booking")
                .then();
    }

    public ValidatableResponse getBookingIdInvalid(){
        return RestAssured.given()
                .log().all()
                .pathParam("id", 443)
                .when()
                .get("booking/{id}")
                .then();
    }

    public ValidatableResponse postBookingID(Map dados){
       return    RestAssured.given()
               .header("Content-type", "application/json")
               .and()
               .body(dados)
               .log().all()
               .when()
               .post("booking")
               .then()
               .log().all();
    }

    public ValidatableResponse putBookingId(Map dados){
        return RestAssured.given()
                .auth()
                .preemptive()
                .basic("admin", "password123")
                .header("Content-type", "application/json")
                .pathParam("id", 15)
                .and()
                .body(dados)
                .log().all()
                .when()
                .put("booking/{id}")
                .then();
    }

    public ValidatableResponse deleteBookingId(Object idValue){
        return  RestAssured.given()
                .auth()
                .preemptive()
                .basic("admin", "password123")
                .header("Content-type", "application/json")
                .pathParam("id", idValue)
                .when()
                .delete("booking/{id}")
                .then();
    }
}
